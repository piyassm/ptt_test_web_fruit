import styled from "styled-components";
import axios from "axios";
import { useEffect, useState } from "react";
import { Modal, Button, Form, Row, Col } from "react-bootstrap";

const AppWrapped = styled.div`
  margin-top: 64px;
`;

let timeOutFetch;

function App() {
  const [dataFruits, setDataFruits] = useState([]);
  const [show, setShow] = useState(false);
  const [textSearch, setTextSearch] = useState("");

  const [formData, setFormData] = useState({
    name: "",
    image: "",
  });
  console.log("formData", formData);
  const handleClose = () => {
    setShow(false);
    setFormData({
      name: "",
      image: "",
    });
  };
  const handleShow = () => setShow(true);

  const handleFormData = (name, e) => {
    setFormData((prev) => ({
      ...prev,
      [name]: e.target.value,
    }));
  };

  const fetchData = async () => {
    const body = {};
    let url = "/show";
    if (!!textSearch?.length) {
      url += `?textSearch=${textSearch}`;
    }
    const response = await axios({
      url: url,
      method: "GET",
      headers: { "Content-Type": "application/json" },
      data: JSON.stringify(body),
    });

    const data = await response.data;
    const { responseRecord } = data;
    setDataFruits(responseRecord);
  };

  useEffect(() => {
    fetchData();
  }, []);

  useEffect(() => {
    clearTimeout(timeOutFetch);
    timeOutFetch = setTimeout(() => {
      fetchData();
    }, 300);
  }, [textSearch]);

  const addData = async () => {
    const validStatus = Object.keys(formData).every((data) => {
      return formData?.[data]?.length;
    });
    if (!validStatus) {
      return;
    }
    const body = formData;
    await axios({
      url: "/add",
      method: "POST",
      headers: { "Content-Type": "application/json" },
      data: JSON.stringify(body),
    });
    handleClose();
    fetchData();
  };

  return (
    <AppWrapped>
      <div className="container">
        <h1>ผลไม้</h1>
        <div className="d-flex justify-content-end">
          <div>
            <button
              type="button"
              className="btn btn-success"
              onClick={handleShow}
            >
              เพิ่มผลไม้
            </button>
          </div>
        </div>
        <div className="d-flex mt-4 justify-content-end">
          <div>
            <input
              type="text"
              className="form-control"
              placeholder="Search here"
              onChange={(e) => setTextSearch(e.target.value)}
            />
          </div>
        </div>

        <div>
          <table className="table">
            <thead>
              <tr>
                <th scope="col" width="25%">
                  ชื่อผลไม้
                </th>
                <th scope="col"></th>
              </tr>
            </thead>
            <tbody>
              {dataFruits.map((data, key) => {
                return (
                  <tr key={`fruit-${key}`}>
                    <td>{data.name}</td>
                    <td>
                      <img src={data.image} alt="" width="150" />
                    </td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </div>
      </div>

      <Modal show={show} onHide={handleClose}>
        <Modal.Header>
          <Modal.Title>Create</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form>
            <Form.Group as={Row} className="mb-3">
              <Form.Label column sm="2">
                Name:
              </Form.Label>
              <Col sm="10">
                <Form.Control
                  type="text"
                  onChange={(e) => handleFormData("name", e)}
                />
              </Col>
            </Form.Group>
            <Form.Group as={Row} className="mb-3">
              <Form.Label column sm="2">
                Photo:
              </Form.Label>
              <Col sm="10">
                <Form.Control
                  type="text"
                  onChange={(e) => handleFormData("image", e)}
                />
              </Col>
            </Form.Group>
          </Form>
          <div className="text-center">
            <Button variant="success" onClick={addData}>
              Save
            </Button>
            <Button variant="light" className="ml-3" onClick={handleClose}>
              Cancle
            </Button>
          </div>
        </Modal.Body>
      </Modal>
    </AppWrapped>
  );
}

export default App;
