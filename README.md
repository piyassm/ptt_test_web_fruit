Demo Site Here:
```sh
https://hidden-earth-64560.herokuapp.com/
```

## Available Scripts

In the project directory

You can run (FRONT END):

### `yarn start-client`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.


You can run (BACK END):

### `yarn start-server`

Runs the app in [http://localhost:5000](http://localhost:5000) 


