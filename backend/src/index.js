const express = require("express");
const cors = require("cors");
const path = require("path");

const app = express();

const buildPath = path.join(__dirname, "../..", "build");
app.use(express.static(buildPath));

app.use(cors());
app.use(express.json());
const PORT = process.env.PORT || 5000;

let mockData = [];

app.get("/show", async (req, res) => {
  const { textSearch } = req.query;

  let mockDataSearch = [...mockData];
  if (textSearch) {
    mockDataSearch = mockDataSearch.filter((data) => {
      return data.name.includes(textSearch);
    });
  }

  const response = {
    responseRecord: mockDataSearch,
  };
  res.json(response);
});

app.post("/add", async (req, res) => {
  const { name, image } = req.body;
  mockData.push({
    name,
    image,
  });

  const response = {
    responseRecord: mockData,
  };
  res.json(response);
});

app.listen(PORT);

console.log(`listening in port ${PORT}`);
